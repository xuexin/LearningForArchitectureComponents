# 写在最前

以下仅仅是我个人的反思,但我一直相信我的思考是有足够价值的,那么,回答下面几个问题:

- 我们的App,从在桌面点击开始,直至进入主界面,或者更深一级的列表界面,我们开启了几个线程,他们又在什么时候结束的?
- 我们不论是谁,git的log里都可以看到优化XX的描述,凭什么说是优化,优化了多少,有没有一个量化的标准?
- 我们有几次monkey跑出ANR/OOM找不到原因,最后不了了之了,定位不了到底是为什么?
- 异常处理,我们有处理所有,哪怕所有会导致crash的异常了吗?
- 现在项目代码对弹窗的管理十分混乱,我们更多的是从需求层面规避多重弹窗的出现,但是从技术上来说,这样合适吗?
- 现在在开发时,完全没有考虑低端高端机器的不同,直接用一刀切的方式进行编码,固然省事,但我们难道就真的不能为低端机做些什么?或者反过来,为高端机做些什么?

本项目在介绍[Architecture Components](https://developer.android.com/topic/libraries/architecture/index.html)的过程中,会尝试一一回答上诉问题