### LiveData 源码分析

> ### LiveData基本用法

```
//声明
LiveData<T> mLiveData=new LiveData<T>()

//注册监听
mLiveData.observe(LifecycleOwner::Class, new Observer<T>() {
            @Override
            public void onChanged(@Nullable t T) {
                //数据改变通知 
            }
        })

```



> ### 源码分析

**LiveData**之所以能感应生命周期，是由**LiveData::observe()**的第一个参数传入了**LifecycleOwner::Class**

```
public interface LifecycleOwner {
    @NonNull
    Lifecycle getLifecycle();
}
```

在之前分析**Lifecycle**的时候知道了，整个**APP**的生命周期，**Activity**的生命周期，**Fragment**的生命周期最后都会交给**LifecycleRegistry::class**。

下面看下它的源码

```
public abstract class Lifecycle {
    @MainThread
    public abstract void addObserver(@NonNull LifecycleObserver observer);
   
    @MainThread
    removeObserver(@NonNull LifecycleObserver observer);
    
    ...
   
    }
```

主要的方法是这两个方法,通过**Lifecycle::class**可以把生命周期很容易的派发到其它类中

```
Lifecycle.addObserver(new LifecycleObserver() {
            @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
            public void onResume() {
                System.out.println("ProcessLifecycleOwner  onResume = ");
            }

            @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
            public void onPause() {
                System.out.println("ProcessLifecycleOwner  onPause = ");
            }

            @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
            public void onStop() {
                System.out.println("ProcessLifecycleOwner  onStop = ");
            }

        });
```

上面的代码就是把生命周期方法派发到了其它的类，别忘了了生命周期结束时，取消监听



**LifecycleRegistry.java**

派发生命周期的流程

```
public void handleLifecycleEvent(@NonNull Lifecycle.Event event) {
        ...
        moveToState(next);
    }
```

往下

```
private void moveToState(State next) {
		...
        sync();
        ...
    }
```

往下

```
private void sync() {
       ....
       forwardPass(lifecycleOwner);
       ...
    }
```

往下

```
private void forwardPass(LifecycleOwner lifecycleOwner) {
        Iterator<Entry<LifecycleObserver, ObserverWithState>> ascendingIterator =
                mObserverMap.iteratorWithAdditions();
        while (ascendingIterator.hasNext() && !mNewEventOccurred) {
            Entry<LifecycleObserver, ObserverWithState> entry = ascendingIterator.next();
            ObserverWithState observer = entry.getValue();
            while ((observer.mState.compareTo(mState) < 0 && !mNewEventOccurred
                    && mObserverMap.contains(entry.getKey()))) {
                pushParentState(observer.mState);
                observer.dispatchEvent(lifecycleOwner, upEvent(observer.mState));
                popParentState();
            }
        }
    }
```

这里面多一个包装类

```
static class ObserverWithState {
         ...
         ObserverWithState(LifecycleObserver observer, State initialState) {
            mLifecycleObserver = Lifecycling.getCallback(observer);
            ...
        }
        void dispatchEvent(LifecycleOwner owner, Event event) {
            ...
            mLifecycleObserver.onStateChanged(owner, event);
            ...
        }
    }
```

通过上面的调用我们明白了，到最后通过**mLifecycleObserver.onStateChanged(owner, event);**把生命周期派发到了每一个监听生命周期的方法.

上面特意多贴出了一个方法

```
static class ObserverWithState {
         ...
         ObserverWithState(LifecycleObserver observer, State initialState) {
            mLifecycleObserver = Lifecycling.getCallback(observer);
            ...
        }
    }
```

**Lifecycling.getCallback(observer)**方法很重要，看下

```
static GenericLifecycleObserver getCallback(Object object) {
        if (object instanceof FullLifecycleObserver) {
            return new FullLifecycleObserverAdapter((FullLifecycleObserver) object);
        }

        if (object instanceof GenericLifecycleObserver) {
            return (GenericLifecycleObserver) object;
        }

        final Class<?> klass = object.getClass();
        int type = getObserverConstructorType(klass);
        if (type == GENERATED_CALLBACK) {
            List<Constructor<? extends GeneratedAdapter>> constructors =
                    sClassToAdapters.get(klass);
            if (constructors.size() == 1) {
                GeneratedAdapter generatedAdapter = createGeneratedAdapter(
                        constructors.get(0), object);
                return new SingleGeneratedAdapterObserver(generatedAdapter);
            }
            GeneratedAdapter[] adapters = new GeneratedAdapter[constructors.size()];
            for (int i = 0; i < constructors.size(); i++) {
                adapters[i] = createGeneratedAdapter(constructors.get(i), object);
            }
            return new CompositeGeneratedAdaptersObserver(adapters);
        }
        return new ReflectiveGenericLifecycleObserver(object);
    }
```

也就是说传监听的**LifecycleObserver**可以有很多种方式，除了上面的注释方法的外还有可以传**FullLifecycleObserverAdapter**，**GenericLifecycleObserver**，好有两个是库里面用的，就不深究了。

好了，把生命周期派发到其它地方的就告一段落了

接下来继续分析**LiveData**感应生命周期

![livedata_hirearchy](./pic/livedata_hirearchy.png)

它有两个子类**MutableLiveData.java**, **MediatorLiveData.java** ,平时用的多的就是**MutableLiveData**，它是可以改变数据的**LiveData**,而**MediatorLiveData**是在没有**LifecycleOwner.java**的时候在中间转换的，也是较难理解的一部分。

```
public class MutableLiveData<T> extends LiveData<T> {
    @Override
    public void postValue(T value) {
        super.postValue(value);
    }

    @Override
    public void setValue(T value) {
        super.setValue(value);
    }
}
```

**MutableLiveData**只有两个方法，**MutableLiveData::postValue**是子线程调用，**MutableLiveData::setValue**是主线程调用。

我们只分析一个方法**setValue()**

```
protected void setValue(T value) {
        ...
        dispatchingValue(null);
    }
```

```
    private void dispatchingValue(@Nullable LifecycleBoundObserver initiator) {
    	...
        considerNotify(initiator);
         ...
    }
```

```
private void considerNotify(LifecycleBoundObserver observer) {
		//如果当期观察者不是活跃的就不会通知，那什么时候通知呢？下面说到
        if (!observer.active) {
            return;
        }
        
        // 这里再判断下LifecycleRegister的状态，加保险的判断
        if (!isActiveState(observer.owner.getLifecycle().getCurrentState())) {
            observer.activeStateChanged(false);
            return;
        }
        if (observer.lastVersion >= mVersion) {
            return;
        }
        observer.lastVersion = mVersion;
        //通知到观察者哪里
        observer.observer.onChanged((T) mData);
    }
```



刚才留有一个疑问就是当数据比带生命周期的组件提前准备好之后怎么样通知？

我们是这样添加一个观察者的

```
@MainThread
    public void observe(@NonNull LifecycleOwner owner, @NonNull Observer<T> observer) {
         ...
        LifecycleBoundObserver wrapper = new LifecycleBoundObserver(owner, observer);
         ...
        owner.getLifecycle().addObserver(wrapper);
    }
```

出现新类**LifecycleBoundObserver.java**

然后把它添加到**LifecycleRegister**

```
class LifecycleBoundObserver implements GenericLifecycleObserver {
        ...
        @Override
        public void onStateChanged(LifecycleOwner source, Lifecycle.Event event) {
            ...
            activeStateChanged(isActiveState(owner.getLifecycle().getCurrentState()));
        }

        void activeStateChanged(boolean newActive) {
            ...
            //当状态为活跃时再次发送通知
            if (active) {
                dispatchingValue(this);
            }
        }
    }
```

```
private void dispatchingValue(@Nullable LifecycleBoundObserver initiator) {
		//如果已经发送了就直接返回
        if (mDispatchingValue) {
            mDispatchInvalidated = true;
            return;
        }
       	....
 }
```



通过上面的分析发现带生命周期的组件要收到数据一定要等到状态为active的时候，如果状态不为active就算数据准备好了也不会发送数据改变的通知。



该轮到**MediatorLiveData.java**了,上面提到了它的用处是在没有**LifecycleOwer**的时候也能监听数据的改变？打个问号，它为什么能做到，不是说数据的通知一定要有生命的组件的在active的时候才能发送通知呢？

先来看它最直接的应用**Transformations.java**

这里类是可以实现，让一个**LiveData**,在没有（最起码看上去没有）**LifecycleOwer**，的时候可以被监听数据变化

看下实现

```
public class Transformations {
	...
    @MainThread
    public static <X, Y> LiveData<Y> map(@NonNull LiveData<X> source,
            @NonNull final Function<X, Y> func) {
        final MediatorLiveData<Y> result = new MediatorLiveData<>();
        result.addSource(source, new Observer<X>() {
            @Override
            public void onChanged(@Nullable X x) {
                result.setValue(func.apply(x));
            }
        });
        return result;
    }
    ...
    }
```

解释下：传入一个**LiveData**，当**LiveData**改变数据的时候调用**func**方法生成数据，然后让返回的**LiveData**发送数据改变的通知（setValue方法调用后就会发送通知给它的观察者）

这里面并没有出现**LifecycleOwer**，它是怎么做到通知的？

让我们看下**MediatorLiveData::addSource**方法

```
 public <S> void addSource(@NonNull LiveData<S> source, @NonNull Observer<S> onChanged) {
        Source<S> e = new Source<>(source, onChanged);
        ...
    }
```

这里出现了**Source.java**,看下是不死它有猫腻

```
private static class Source<V> implements Observer<V> {
        final LiveData<V> mLiveData;
        final Observer<V> mObserver;
        int mVersion = START_VERSION;

        Source(LiveData<V> liveData, final Observer<V> observer) {
            mLiveData = liveData;
            mObserver = observer;
        }

        void plug() {
            mLiveData.observeForever(this);
        }

        void unplug() {
            mLiveData.removeObserver(this);
        }

        @Override
        public void onChanged(@Nullable V v) {
            if (mVersion != mLiveData.getVersion()) {
                mVersion = mLiveData.getVersion();
                mObserver.onChanged(v);
            }
        }
    }
```

有个方法**mLiveData.observeForever(this)**，forever这里很神奇，去看看

```
@MainThread
    public void observeForever(@NonNull Observer<T> observer) {
        observe(ALWAYS_ON, observer);
    }
```

```
private static final LifecycleOwner ALWAYS_ON = new LifecycleOwner() {

        private LifecycleRegistry mRegistry = init();

        private LifecycleRegistry init() {
            LifecycleRegistry registry = new LifecycleRegistry(this);
            registry.handleLifecycleEvent(Lifecycle.Event.ON_CREATE);
            registry.handleLifecycleEvent(Lifecycle.Event.ON_START);
            registry.handleLifecycleEvent(Lifecycle.Event.ON_RESUME);
            return registry;
        }

        @Override
        public Lifecycle getLifecycle() {
            return mRegistry;
        }
    };
```

这下明白了

**LiveData**内部有个静态的私有的**ALWAYS_ON**，其实就是**LifecycleRegistry**，而且一开始它的状态就是活跃的，通过**init**实现活跃状态，所以通过它的生命周期感知的**LiveData** 的**Observer**都可以收到数据的变化通知。但是，使用**ALWAYS_ON**是有风险的，因为它不是真正的生命周期组件，所以一定要记得取消监听。



> ### 总结

这篇是大体上分析了**LiveData**的机制，从总流程出发的，里面有些逻辑处理没有很详细的分析。

