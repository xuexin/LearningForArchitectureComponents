## ViewModel 生命周期

> ### 跟Activity生命周期一致

```
ViewModelProviders.of(getActivity()).get(ProductViewModel.class)
```

这是获取继承**AndroidViewModel**的**ViewModel**。

也就是构造方法要有

```
public xxx(@NonNull Application application)
```

如果不是的话就要使用

```
ViewModelProviders.of(getActivity(),factory).get(ProductListViewModel.class)
```

带自定义**ViewModelFactory**的**of**方法

**ViewModel**的生命周期大于**Activity**是指当当前堆栈在前台配置发生改变**Activity**重建时可以保存实例

当调用**ViewModelProviders**的**of**方法时会做些初始化工作

```
 @MainThread
    public static ViewModelProvider of(@NonNull FragmentActivity activity) {
        initializeFactoryIfNeeded(checkApplication(activity));
        return new ViewModelProvider(ViewModelStores.of(activity), sDefaultFactory);
    }
```

```
ViewModelStore.java

 @MainThread
    public static ViewModelStore of(@NonNull FragmentActivity activity) {
        return holderFragmentFor(activity).getViewModelStore();
    }
```

 ```
@RestrictTo(RestrictTo.Scope.LIBRARY_GROUP)
    public static HolderFragment holderFragmentFor(FragmentActivity activity) {
        return sHolderFragmentManager.holderFragmentFor(activity);
    }
 ```

终于出现了**HolderFragment**就是它来保存**ViewModel**实例的

```
HolderFragment holderFragmentFor(FragmentActivity activity) {
            FragmentManager fm = activity.getSupportFragmentManager();
            HolderFragment holder = findHolderFragment(fm);
            if (holder != null) {
                return holder;
            }
            holder = mNotCommittedActivityHolders.get(activity);
            if (holder != null) {
                return holder;
            }

            if (!mActivityCallbacksIsAdded) {
                mActivityCallbacksIsAdded = true;
                activity.getApplication().registerActivityLifecycleCallbacks(mActivityCallbacks);
            }
            holder = createHolderFragment(fm);
            mNotCommittedActivityHolders.put(activity, holder);
            return holder;
        }
```

上面的代码就是说如果没有把**HolderFragment**加到**FragmentManager**就调用**createHolderFragment（）**

```
private static HolderFragment createHolderFragment(FragmentManager fragmentManager) {
            HolderFragment holder = new HolderFragment();       fragmentManager.beginTransaction().add(holder,HOLDER_TAG).commitAllowingStateLoss();
            return holder;
        }
```

就是把**HolderFragment**加到**FragmentManager**

看下构造方法

```

public HolderFragment() {
        setRetainInstance(true);
    }
```

之所以**ViewModel**的实例的生命周期比**Activity**长就是因为**HolderFragment::setRetainInstance(true)**

这个就不解释了

```
@RestrictTo(RestrictTo.Scope.LIBRARY_GROUP)
public class HolderFragment extends Fragment {
	...
    private ViewModelStore mViewModelStore = new ViewModelStore();
    ...
    }
```

而它里面有个**ViewModelStore**，它就是用来存**ViewModel**的

```
public class ViewModelStore {

    private final HashMap<String, ViewModel> mMap = new HashMap<>();

    final void put(String key, ViewModel viewModel) {
        ViewModel oldViewModel = mMap.get(key);
        if (oldViewModel != null) {
            oldViewModel.onCleared();
        }
        mMap.put(key, viewModel);
    }

    final ViewModel get(String key) {
        return mMap.get(key);
    }

    /**
     *  Clears internal storage and notifies ViewModels that they are no longer used.
     */
    public final void clear() {
        for (ViewModel vm : mMap.values()) {
            vm.onCleared();
        }
        mMap.clear();
    }
}
```



代码很少，一看就明白了



现在来看下**ViewModel**的生命周期

```
public abstract class ViewModel {
/**
     * This method will be called when this ViewModel is no longer used and will be destroyed.
     * <p>
     * It is useful when ViewModel observes some data and you need to clear this subscription to
     * prevent a leak of this ViewModel.
     */
    protected void onCleared() {
    }
}
```

很简单的就一个方法，特意把注释贴出来了

这个方法是在**HolderFragment::onDestroy**调用

```
public class HolderFragment extends Fragment {
	...
    private ViewModelStore mViewModelStore = new ViewModelStore();

   ...

    @Override
    public void onDestroy() {
        super.onDestroy();
        mViewModelStore.clear();
    }
    ...
    }
```



> ## 跟Fragment的生命周期

跟**Activity**的原理是一样的，只不过获取的时候变成了**Fragment**

```
ViewModelProviders.of(fragment).get(ProductViewModel.class);
```

之后的流程都跟**Activity**一样，生命周期也比**Fragment**长，当配置发生改变而导致**Fragment**重建时**ViewModel**实例可以保存



> ## 总结

到这里**ViewModel**的生命周期就走完了

保存**ViewModel**的实例是保存在**HolderFragment**，然而**HolderFragment**设置了**HolderFragment::setRetainInstance**, 所以当配置发生改变而导致**Activity**重启时实例可以保存

