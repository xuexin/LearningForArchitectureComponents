### Lifecycle生命周期 

> **初始化** 
```java
<provider
            android:name="android.arch.lifecycle.LifecycleRuntimeTrojanProvider"
            android:exported="false"
            android:multiprocess="true"
            android:authorities="com.example.android.persistence.lifecycle-trojan" />
```

由``ProcessLifecycleOwnerInitializer.java ``开始

```
class ProcessLifecycleOwnerInitializer extends ContentProvider

@Override
 public boolean onCreate() {
        LifecycleDispatcher.init(getContext());
        ProcessLifecycleOwner.init(getContext());
        return true;
 }
```

采用了**ContentProvider**进行初始化，这样做的好处是可以不用再**Application**里面进行初始化（**ContentProvider**的**onCreate**方法在**Application**的**attachBaseContext**之后**onCreate**之前)

**ProcessLifecycleOwnerInitializer::onCreate**做了两件事

1. LifecycleDispatcher.init(getContext());

   所有**Activity**的生命周期的监听派发

2. ProcessLifecycleOwner.init(getContext());

   整个应用的生命周期派发，不是所有生命周期方法 只有 start resume pause stop，

   start resume 应用启动了在前台

   pause 可见状态

   stop 退出了应用 或者在后台



> ##整个应用 生命周期感知的神秘面纱

```
 ProcessLifecycleOwner.init(getContext());
```

```
void attach(Context context) {
        mHandler = new Handler();
        mRegistry.handleLifecycleEvent(Lifecycle.Event.ON_CREATE);
        Application app = (Application) context.getApplicationContext();
        app.registerActivityLifecycleCallbacks(new EmptyActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
                ReportFragment.get(activity).setProcessListener(mInitializationListener);
            }

            @Override
            public void onActivityPaused(Activity activity) {
                activityPaused();
            }

            @Override
            public void onActivityStopped(Activity activity) {
                activityStopped();
            }
        });
    }
```

```
private ActivityInitializationListener mInitializationListener =
            new ActivityInitializationListener() {
                @Override
                public void onCreate() {
                }

                @Override
                public void onStart() {
                    activityStarted();
                }

                @Override
                public void onResume() {
                    activityResumed();
                }
            };
```

从上面可以很清晰的看到监听了每个**Activity**的四个生命周期方法

如果要监听应用的生命周期示例如下(可以放到任何地方)

 ```
ProcessLifecycleOwner.get().getLifecycle().addObserver(new LifecycleObserver() {
            @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
            public void onResume() {
                System.out.println("ProcessLifecycleOwner  onResume = ");
            }

            @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
            public void onPause() {
                System.out.println("ProcessLifecycleOwner  onPause = ");
            }

            @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
            public void onStop() {
                System.out.println("ProcessLifecycleOwner  onStop = ");
            }

        });
 ```







> ##Activity的生命周期感知的神秘面纱

```
LifecycleDispatcher.init(getContext());
```

```
static void init(Context context) {
        if (sInitialized.getAndSet(true)) {
            return;
        }
        ((Application) context.getApplicationContext())
                .registerActivityLifecycleCallbacks(new DispatcherActivityCallback());
    }
```

初始化方法，同样通过**Application::registerActivityLifecycleCallbacks**来监听所有**Activity**的生命周期

```
DispatcherActivityCallback.java
```

```
 		@Override
        public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
            if (activity instanceof FragmentActivity) {
                ((FragmentActivity) activity).getSupportFragmentManager()
                        .registerFragmentLifecycleCallbacks(mFragmentCallback, true);
            }
            ReportFragment.injectIfNeededIn(activity);
        }
        
        @Override
        public void onActivityStopped(Activity activity) {
            if (activity instanceof FragmentActivity) {
                markState((FragmentActivity) activity, CREATED);
            }
        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
            if (activity instanceof FragmentActivity) {
                markState((FragmentActivity) activity, CREATED);
            }
        }
```

**DispatcherActivityCallback::onActivityCreated**方法是监听Activity生命周期的关键代码，**LifecycleDispatcher**不是通过**DispatcherActivityCallback**来监听**Activity**的所有生命周期方法，是通过给**Activity**添加一个**Fragment**来监听**Activity** 的生命周期，同**Glide**的做法是一样的。

```
if (activity instanceof FragmentActivity) {
                ((FragmentActivity) activity).getSupportFragmentManager()
                        .registerFragmentLifecycleCallbacks(mFragmentCallback, true);
            }
```

这个代码是处理**Fragment**相关的先放着分析到**Fragment**生命周期是再分析

```
ReportFragment.injectIfNeededIn(activity);
```

这个代码是用来感知**Activity**生命周期，这里有个新成员**ReportFragment**

```
public static void injectIfNeededIn(Activity activity) {
        android.app.FragmentManager manager = activity.getFragmentManager();
        if (manager.findFragmentByTag(REPORT_FRAGMENT_TAG) == null) {
            manager.beginTransaction().add(new ReportFragment(),									REPORT_FRAGMENT_TAG).commit();
            manager.executePendingTransactions();
        }
    }
```

**ReportFragment::injectIfNeededIn**就是把自己add到**Activity**里面，然后就与**Activity**同生共死了

接下来分析一个生命周期的回调，其它都是一样一样的

```
ReportFragme.java
```

```
  @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        dispatch(Lifecycle.Event.ON_CREATE);
    }
```

```
private void dispatch(Lifecycle.Event event) {
        Activity activity = getActivity();
        if (activity instanceof LifecycleRegistryOwner) {
            ((LifecycleRegistryOwner)activity).getLifecycle().handleLifecycleEvent(event);
            return;
        }

        if (activity instanceof LifecycleOwner) {
            Lifecycle lifecycle = ((LifecycleOwner) activity).getLifecycle();
            if (lifecycle instanceof LifecycleRegistry) {
                ((LifecycleRegistry) lifecycle).handleLifecycleEvent(event);
            }
        }
    }
```

可以看到在**dispatch**方法里面把感知到的生命周期方法转交给了**LifecycleRegistryOwner**

```
/**
 * @deprecated Use {@code android.support.v7.app.AppCompatActivity}
 * which extends {@link LifecycleOwner}, so there are no use cases for this class.
 */
@SuppressWarnings({"WeakerAccess", "unused"})
@Deprecated
public interface LifecycleRegistryOwner extends LifecycleOwner {
    @NonNull
    @Override
    LifecycleRegistry getLifecycle();
}
```

一个接口 还是被标**@Deprecated**，因为在**SupportActivity**和**Fragment**都有实现了

**LifecycleOwner**是所有感知生命周期的桥梁，先放着，到**LiveData**时再分析



> ##Fragment生命周期的神秘面纱



在分析**Activity**生命周期的时候我们提到了**Fragment**生命周期的监听

```
 @Override
        public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
            if (activity instanceof FragmentActivity) {
                ((FragmentActivity) activity).getSupportFragmentManager()
                        .registerFragmentLifecycleCallbacks(mFragmentCallback, true);
            }
            ...
            ...
        }
```

通过**SupportFragmentManager::registerFragmentLifecycleCallbacks**传到**FragmentCallback**

```
@SuppressWarnings("WeakerAccess")
    @VisibleForTesting
    static class FragmentCallback extends FragmentManager.FragmentLifecycleCallbacks {

        @Override
        public void onFragmentCreated(FragmentManager fm, Fragment f, Bundle savedInstanceState) {
            dispatchIfLifecycleOwner(f, ON_CREATE);

            if (!(f instanceof LifecycleRegistryOwner)) {
                return;
            }

            if (f.getChildFragmentManager().findFragmentByTag(REPORT_FRAGMENT_TAG) == null) {
                f.getChildFragmentManager().beginTransaction().add(new DestructionReportFragment(),
                        REPORT_FRAGMENT_TAG).commit();
            }
        }

        @Override
        public void onFragmentStarted(FragmentManager fm, Fragment f) {
            dispatchIfLifecycleOwner(f, ON_START);
        }

        @Override
        public void onFragmentResumed(FragmentManager fm, Fragment f) {
            dispatchIfLifecycleOwner(f, ON_RESUME);
        }
    }
```

看最主要的代码

```
if (!(f instanceof LifecycleRegistryOwner)) {
      return;
 }
```

如果遇到**Fragment**没有实现**LifecycleRegistryOwner**就返回，因为**LifecycleRegistryOwner**已经被废弃了，所以最新官方架构里面到这里就结束了，那那那。。。。**Fragment**的生命周期怎么监听呢，怎么就结束了。

因为**Fragment**的生命周期派发被自己给接管了

```
public class Fragment implements ComponentCallbacks, OnCreateContextMenuListener, LifecycleOwner{
	...
 LifecycleRegistry mLifecycleRegistry = new LifecycleRegistry(this);
	...
  @Override
    public Lifecycle getLifecycle() {
        return mLifecycleRegistry;
    }
    
    ...
    
    void performCreate(Bundle savedInstanceState) {
       ...
        mLifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_CREATE);
    }
    
    ...

}
```

直接把自己生命周期给派发出去了

但是是官方的最新包才会哦，包括**andriod.app.Fragment**也是不会的，如果你的**Fragment**是继承**andriod.app.Fragment**就要自己实现**LifecycleOwner**，然后派发生命周期了

下面是自己派发生命周期的示例代码

```
    private LifecycleRegistry mLifecycleRegistry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mLifecycleRegistry = new LifecycleRegistry(this);
        mLifecycleRegistry.markState(Lifecycle.State.CREATED);
    }

    @Override
    public void onStart() {
        super.onStart();
        mLifecycleRegistry.markState(Lifecycle.State.STARTED);
    }

    @NonNull
    @Override
    public Lifecycle getLifecycle() {
        return mLifecycleRegistry;
    }
}

```





## 总结

通过上面的分析我们知道了**LifecycleOwner**是怎么感知到生命周期的，因为**LifecycleOwner**是感知生命周期和被感知生命周期的桥梁，到了**LifecycleOwner**就可以传到具体感知生命周期的地方

生命周期分三类

1. App生命周期：由**ProcessLifecycleOwner**实现
2. Activity生命周期：由**ReportFragment**实现
3. Fragment生命周期：自己实现



















